@extends('blog::layouts.standard')

@section('main')
  @include('pages::components.sliders.internal', ['images' => $category->images, 'page' => $category])

  @include('blog::components.articles', ['category' => $category])
@stop

@section('inline_js')
<script>
var $grid = $('.blog__articles .grid').isotope({
  itemSelector: '.grid-item',
  layoutMode: 'fitRows',
  columnWidth: 200
});

$('.blog__articles .filter a').on( 'click', function() {
  var filterValue = $(this).attr('data-filter');
  console.log(filterValue);
  $grid.isotope({ filter: filterValue });
});
</script>
@stop
