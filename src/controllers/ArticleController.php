<?php namespace WorkInProgress\ClientBlog;

class ArticleController extends \BaseController {

  public function __construct()
  {
    $this->beforeFilter('csrf', array('on' => 'post'));
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getArticle($full_permalink = 'articles')
  {
    //Look for a category
    $category = Category::whereRaw('full_permalink = ? and active = 1', [$full_permalink])->first();

    if($category) {
      $data = [
        'category' => $category,
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'articles')->first(),
        'title' => $category->full_title
      ];

      //Display category
      return \View::make('blog::' . str_plural($category->template->src), $data);
    }

    //Look for a article instead
    $article = Article::whereRaw('full_permalink = ? and active = 1', [$full_permalink])->first();

    //Grab the recent articles
    $recent_articles = Article::all()->reverse()->take(5);

    if($article) {
      $data = [
        'article' => $article,
        'recent_articles' => $recent_articles,
        'pages' => \WorkInProgress\ClientPages\Page::find(1),
        'page' => \WorkInProgress\ClientPages\Page::where('permalink', '=', 'articles')->first(),
        'title' => $article->full_title
      ];

      //Grab the prev/next article
      $previous_id = Article::where('id', '<', $article->id)->max('id');
      $next_id = Article::where('id', '>', $article->id)->min('id');

      if($previous_id) {
        $data['previous_article'] = Article::findOrFail($previous_id);
      }

      if($next_id) {
        $data['next_article'] = Article::findOrFail($next_id);
      }

      //Display article 
      return \View::make('blog::' . $article->template->src, $data);
    }

  }

}

?>
