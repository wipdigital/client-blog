<?php namespace WorkInProgress\ClientBlog;

class ArticleImage extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_article_images';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

  public function parentArticle()
  {
    return $this->belongsTo('\WorkInProgress\ClientBlog\Article', 'blog_article_id');
  }

  public function getHeightAttribute()
  {
    $size = getimagesize(public_path() . $this->attributes['src']);

    return count($size) ? $size[1] : null;
  }

  public function getWidthAttribute()
  {

    $size = getimagesize(public_path() . $this->attributes['src']);

    return count($size) ? $size[0] : null;
  }

}

?>
