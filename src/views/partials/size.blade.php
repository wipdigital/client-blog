<div class="size options">
  <p class="option-label">Select Size</p>
  @if($product->options()->size()->count()) 
    @foreach($product->options()->size()->get() as $option)
      <a href="#" class="@if(Config::get('ecommerce::product.stock_control')) disabled @endif" data-id="{{ $option->id }}" title="{{ $option->title }}">{{ substr($option->title, 0, 1) }}</a>
    @endforeach

    {{ Form::hidden('size_id', null, ['id' => 'size_id']) }}
  @else
    <a href="#" title="One size fits most" class="active">A</a>
  @endif
</div>
