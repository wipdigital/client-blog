@if(count($category->categories))
<h1>{{ $category->title }}</h1>
<div class="ecommerce__categories">
  <div class="row">
  @foreach($categories as $category)
    <div class="small-12 medium-6 large-4 columns">
      <figure class="effect-bubba">
        <a href="/products/{{ $category->full_permalink }}" title="{{ $category->title }}">
          <img src="{{ $category->images()->first() ? Config::get('ecommerce::product.cdn') . $category->images()->first()->src . Config::get('ecommerce::product.query') : '' }}" alt="{{ $category->title }}" class="wow flipInY" width="300" height="300">
          <figcaption>
            <h3>{{ $category->title }}</h3>
            <p>view <strong>now</strong></p>
           </figcaption>   
        </a>
      </figure>
    </div>
  @endforeach
</div>
<div class="clear"></div>
@endif
