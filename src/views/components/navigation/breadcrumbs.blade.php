<div class="navigation__breadcrumbs">
  <nav class="breadcrumbs" role="menubar" aria-label="breadcrumbs">
    <a href="/">Home</a>
    @if(count($resource->breadcrumbs))
    @foreach($resource->breadcrumbs as $category)
    <a href="/articles/{{ $category->blog_category_id ? $category->full_permalink : '' }}">{{ $category->title }}</a>
    @endforeach
    @endif
    <a href="/articles/{{ $resource->blog_category_id ? $resource->full_permalink : '' }}" class="current">{{ $resource->title }}</a>
  </nav>
</div>
