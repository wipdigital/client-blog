<?php

Route::get('/articles', 'WorkInProgress\ClientBlog\ArticleController@getArticle');
Route::get('/articles/{full_permalink}', 'WorkInProgress\ClientBlog\ArticleController@getArticle')->where('full_permalink', '(.*)');

?>
