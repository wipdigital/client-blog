<div class="blog__articles">
  <div class="filter">
    <ul>
      <li class="active"><a href="#" data-filter="*">All</a></li>
      @foreach($category->categories as $cat)
      <li><a href="#" data-filter=".{{ $cat->permalink }}">{{ $cat->seo_title ?: $cat->title }}</a></li>
      @endforeach
    </ul>
  </div>

  <div class="grid">
  @foreach($category->categories as $cat)
    @foreach($cat->articles as $article)
      <div class="grid-item {{ $article->parent_category->permalink }}">
        <figure class="effect-bubba">
          <a href="/articles/{{ $article->full_permalink }}" title="{{ $article->title }}">
            <img src="{{ $article->images()->first() ? Config::get('ecommerce::product.cdn') . $article->images()->first()->src . Config::get('ecommerce::product.query') : '' }}" alt="{{ $article->title }}" class="wow flipInY" width="300" height="300">
            <figcaption>
              <h3>{{ $article->title }}</h3>
              <p>view <strong>now</strong></p>
            </figcaption>
          </a>
        </figure>
      </div>
    @endforeach
  @endforeach
  </div>
</div>
