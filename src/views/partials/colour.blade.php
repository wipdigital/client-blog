<div class="colour options">
  <p class="option-label">Select Colour</p>
  @if($product->options()->colour()->count()) 
    @foreach($product->options()->colour()->active()->get() as $option)
      <img data-id="{{ $option->id }}" src="{{ Config::get('ecommerce::product.cdn') . $option->thumbnail_src . Config::get('ecommerce::product.query') }}" alt="{{ $option->title }}" class="wow flipInY @if(Config::get('ecommerce::product.stock_control')) disabled @endif" width="50" height="50">
    @endforeach

    {{ Form::hidden('colour_id', null, ['id' => 'colour_id']) }}
  @else
    @if($product->images()->first())
      <img src="{{ Config::get('ecommerce::product.cdn') . $product->images()->first()->src . Config::get('ecommerce::product.query') }}" alt="{{ $product->images()->first()->title }}" class="wow flipInY @if(Config::get('ecommerce::product.stock_control')) disabled @endif" width="50" height="50">
    @endif
  @endif

</div>
