<?php namespace WorkInProgress\ClientBlog;

class CategoryImage extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_category_images';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\ClientBlog\Category', 'blog_category_id');
  }

}

?>
