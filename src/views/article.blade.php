@extends('blog::layouts.standard')

@section('main')
  @include('pages::components.sliders.internal', ['images' => $article->images, 'page' => $article])

  @include('blog::components.navigation.breadcrumbs', ['resource' => $article])

  <article>
  {{ $article->description }}
  </article>
@stop
