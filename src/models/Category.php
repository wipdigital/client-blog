<?php namespace WorkInProgress\ClientBlog;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Category extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_categories';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['blog_category_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'active', 'order', 'template_id'];

  public function template()
  {
    return $this->belongsTo('\WorkInProgress\ClientBlog\Template', 'template_id');
  }

  public function articles()
  {
    return $this->hasMany('\WorkInProgress\ClientBlog\Article', 'blog_category_id');
  }

  public function categories()
  {
    return $this->hasMany('\WorkInProgress\ClientBlog\Category', 'blog_category_id');
  }

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\ClientBlog\Category', 'blog_category_id');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\ClientBlog\CategoryImage', 'blog_category_id');
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true)->orderBy('order');
  }

}

?>
