<div class="row">
  <div class="small-12 medium-6 columns">
    <div class="payment-card">
      <a href="/payment/creditcard" class="button expand">Credit Card</a>
    </div>
  </div>
  <div class="small-12 medium-6 columns">
    {{ Form::open(['url' => '/payment/cod', 'class' => 'payment-card', 'method' => 'POST']) }}
      {{ Form::submit('Pay On Site', ['class' => 'button expand']) }}
    {{ Form::close() }}
  </div>
</div>

