<?php namespace WorkInProgress\ClientBlog;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Eloquent\Collection;

class Article extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_articles';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['blog_category_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'url', 'featured', 'active', 'order', 'template_id', 'premium', 'featured'];

  public function template()
  {
    return $this->belongsTo('\WorkInProgress\ClientBlog\Template', 'template_id');
  }

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\ClientBlog\Category', 'blog_category_id');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\ClientBlog\ArticleImage', 'blog_article_id');
  }

  public function getBreadcrumbsAttribute()
  {
    $collection = new Collection;

    for($category = $this->parent_category; isset($category->id); $category = $category->parent_category)
    {
      $collection->add($category);
    }

    return $collection->reverse();
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }

  public function scopeActive($query)
  {
    return $query->where('active', '=', true)->orderBy('order');
  }

  public function getThumbnailImageAttribute()
  {
    return $this->images->count() ? $this->images->get(0) : null;
  }

  public function getBackgroundImageAttribute()
  {
    if(!$this->images->count()) {
      return null;
    }

    return ($this->images->count() >=2) ? $this->images->get(1) : $this->images->get(0);
  }

}

?>
